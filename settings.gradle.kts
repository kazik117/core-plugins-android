pluginManagement {
    includeBuild("plugin")

    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}

plugins {
    id("pl.mkazik.core.config")
}

config_sdk {
    compileSdk.set(34)
    targetSdk.set(33)
}

rootProject.name = "Core Plugins Android Root"
include(":samples:app", ":samples:examplelib", ":samples:kotlinlib")
