# Core Plugins Android [![pipeline status](https://gitlab.com/mkazik-software/core-plugins-android/badges/main/pipeline.svg)](https://gitlab.com/mkazik-software/core-plugins-android/-/commits/main) [![Latest Release](https://gitlab.com/mkazik-software/core-plugins-android/-/badges/release.svg)](https://gitlab.com/mkazik-software/core-plugins-android/-/releases)

This project provides a set of precompiled script plugins for Android applications and libraries. It defaults many options, so you can always configure what needed for your module.

### This project is in an early development

## License

This project is licensed under the Apache 2.0 License - see the LICENSE.md file for details
