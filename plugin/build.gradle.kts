import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    `kotlin-dsl`
    `maven-publish`
    signing
    id("io.github.gradle-nexus.publish-plugin") version "2.0.0"
}

dependencies {
    api("com.android.application:com.android.application.gradle.plugin:8.4.1")
    api("com.android.library:com.android.library.gradle.plugin:8.4.1")
    api("org.jetbrains.kotlin.jvm:org.jetbrains.kotlin.jvm.gradle.plugin:1.9.10")
    api("org.jetbrains.kotlin.android:org.jetbrains.kotlin.android.gradle.plugin:1.9.10")
    api("androidx.compose.compiler:compiler:1.5.3")
}

group = findProperty("POM_GROUP").toString()
version = findProperty("POM_VERSION").toString()

val sourcesJar by tasks.registering(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

val javadocJar by tasks.registering(Jar::class) {
    archiveClassifier.set("javadoc")
}

gradlePlugin {
    plugins {
        register("config") {
            id = "pl.mkazik.core.config"
            implementationClass = "ConfigurationPlugin"
            displayName = "Configuration Settings Plugin"
            description = "Provides a way to configure minSdk, compileSdk and targetSdk"
        }

        register("app") {
            id = "pl.mkazik.core.android-app"
            implementationClass = "AndroidAppConventionPlugin"
            displayName = "Application plugin"
            description = "Provides a main application project plugin"
        }

        register("lib") {
            id = "pl.mkazik.core.android-lib"
            implementationClass = "AndroidLibConventionPlugin"
            displayName = "Library plugin"
            description = "Provides a library project plugin"
        }

        register("kotlin") {
            id = "pl.mkazik.core.kotlin"
            implementationClass = "KotlinLibConventionPlugin"
            displayName = "Kotlin library plugin"
            description = "Provides pure kotlin project plugin"
        }

        register("compose") {
            id = "pl.mkazik.core.compose"
            implementationClass = "ComposeConventionPlugin"
            displayName = "Compose plugin"
            description = "Provides compose plugin configuration"
        }
    }
}

publishing {
    afterEvaluate {
        publications {
            getByName<MavenPublication>("pluginMaven") {
                artifact(sourcesJar.get())
                artifact(javadocJar.get())

                pom {
                    name.set(findProperty("POM_NAME").toString())
                    description.set(findProperty("POM_DESCRIPTION").toString())
                    url.set(findProperty("SCM_URL").toString())
                }
            }

            withType<MavenPublication> {
                pom {
                    url.set(findProperty("SCM_URL").toString())

                    scm {
                        connection.set(findProperty("SCM_DEV_URL").toString())
                        developerConnection.set(findProperty("SCM_DEV_URL").toString())
                        url.set(findProperty("SCM_URL").toString())
                    }

                    licenses {
                        license {
                            name.set(findProperty("LICENSE").toString())
                            url.set(findProperty("LICENSE_URL").toString())
                        }
                    }

                    developers {
                        developer {
                            id.set(findProperty("DEV_ID").toString())
                            name.set(findProperty("DEV_NAME").toString())
                            email.set(findProperty("DEV_MAIL").toString())
                        }
                    }
                }
            }
        }
    }
}

nexusPublishing {
    repositories {
        sonatype {
            nexusUrl.set(uri("https://s01.oss.sonatype.org/service/local/"))
            snapshotRepositoryUrl.set(uri("https://s01.oss.sonatype.org/content/repositories/snapshots/"))
            username.set(findProperty("ossrhUser").toString())
            password.set(findProperty("ossrhPassword").toString())
            stagingProfileId.set(findProperty("ossrhProfile").toString())
        }
    }
}

signing {
    val signingKey = findProperty("signing_key").toString().trim()
    val signingPassword = findProperty("signing_password").toString()
    useInMemoryPgpKeys(signingKey, signingPassword)
    sign(publishing.publications)
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_17.toString()
    }
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}
