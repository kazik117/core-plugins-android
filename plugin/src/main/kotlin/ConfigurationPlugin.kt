import org.gradle.api.Action
import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.initialization.Settings
import org.gradle.api.initialization.resolve.RepositoriesMode
import org.gradle.api.plugins.ExtensionAware
import org.gradle.api.provider.Property
import org.gradle.kotlin.dsl.create

interface ConfigExtension {
    val minSdk: Property<Int>
    val compileSdk: Property<Int>
    val targetSdk: Property<Int>
    val javaVersion: Property<JavaVersion>
    val composeCompiler: Property<String>
}

class ConfigurationPlugin : Plugin<Settings> {
    override fun apply(settings: Settings) {
        settings.dependencyResolutionManagement {
            repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
            repositories {
                google()
                mavenCentral()
            }
        }

        val extension = settings.extensions.create<ConfigExtension>("config_sdk")
        extension.minSdk.convention(28)
        extension.compileSdk.convention(33)
        extension.targetSdk.convention(extension.compileSdk)
        extension.javaVersion.convention(JavaVersion.VERSION_17)
        extension.composeCompiler.convention("1.5.3")
        settings.gradle.rootProject {
            val projConfig = rootProject.extensions.create<ConfigExtension>("config_sdk")
            projConfig.minSdk.set(extension.minSdk)
            projConfig.compileSdk.set(extension.compileSdk)
            projConfig.targetSdk.set(extension.targetSdk)
            projConfig.javaVersion.set(extension.javaVersion)
            projConfig.composeCompiler.set(extension.composeCompiler)
        }
    }
}

fun Settings.config_sdk(action: Action<ConfigExtension>): Unit =
    (this as ExtensionAware).extensions.configure("config_sdk", action)
