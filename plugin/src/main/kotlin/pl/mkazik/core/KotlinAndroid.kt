package pl.mkazik.core

import ConfigExtension
import com.android.build.api.dsl.CommonExtension
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.kotlin.dsl.configure
import org.gradle.kotlin.dsl.withType
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

/**
 * Configure base kotlin with Android options
 */
internal fun Project.configureKotlinAndroid(
    commonExtension: CommonExtension<*, *, *, *, *, *>,
    configExtension: ConfigExtension
) {
    commonExtension.apply {
        compileSdk = configExtension.compileSdk.get()

        defaultConfig {
            minSdk = configExtension.minSdk.get()
        }

        compileOptions {
            sourceCompatibility = configExtension.javaVersion.get()
            targetCompatibility = configExtension.javaVersion.get()
        }
    }

    configureKotlin(configExtension)
}

internal fun Project.configureCompose(
    commonExtension: CommonExtension<*, *, *, *, *, *>,
    configExtension: ConfigExtension
) {
    commonExtension.apply {
        buildFeatures { compose = true }

        composeOptions { kotlinCompilerExtensionVersion = configExtension.composeCompiler.get() }
    }
}

/**
 * Configure base kotlin with JVM options (non-android)
 */
internal fun Project.configureKotlinJvm(
    configExtension: ConfigExtension
) {
    extensions.configure<JavaPluginExtension> {
        sourceCompatibility = configExtension.javaVersion.get()
        targetCompatibility = configExtension.javaVersion.get()
    }

    configureKotlin(configExtension)
}

private fun Project.configureKotlin(
    configExtension: ConfigExtension
) {
    tasks.withType<KotlinCompile>().configureEach {
        kotlinOptions {
            jvmTarget = configExtension.javaVersion.get().toString()
        }
    }
}
