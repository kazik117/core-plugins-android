import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.getByType
import pl.mkazik.core.configureKotlinJvm

class KotlinLibConventionPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with(target) {
            with(pluginManager) {
                apply("org.jetbrains.kotlin.jvm")
            }

            val config = rootProject.extensions.getByType<ConfigExtension>()
            configureKotlinJvm(config)
        }
    }
}
