import com.android.build.api.dsl.ApplicationExtension
import com.android.build.api.dsl.LibraryExtension
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.configure
import org.gradle.kotlin.dsl.findByType
import org.gradle.kotlin.dsl.getByType
import pl.mkazik.core.configureCompose

class ComposeConventionPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with(target) {
            if (extensions.findByType<LibraryExtension>() == null
                && extensions.findByType<ApplicationExtension>() == null) {
                throw IllegalStateException("Android library or app plugin not applied")
            }
            val config = rootProject.extensions.getByType<ConfigExtension>()
            if (extensions.findByType<LibraryExtension>() != null) {
                extensions.configure<LibraryExtension> {
                    configureCompose(this, config)
                }
            }

            if (extensions.findByType<ApplicationExtension>() != null) {
                extensions.configure<ApplicationExtension> {
                    configureCompose(this, config)
                }
            }
        }
    }
}
