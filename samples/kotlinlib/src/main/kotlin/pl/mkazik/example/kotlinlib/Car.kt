package pl.mkazik.example.kotlinlib

data class Car(val model: String, val engine: Engine)

enum class Engine {
    PETROL,
    GAS,
    ELECTRIC,
    HYBRID
}
