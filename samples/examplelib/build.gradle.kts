plugins {
    id("pl.mkazik.core.android-lib")
    id("pl.mkazik.core.compose")
}

android {
    namespace = "pl.mkazik.example.lib"
}

dependencies {
    api(project(":samples:kotlinlib"))

    implementation("androidx.core:core-ktx:1.7.0")
    implementation("com.google.android.material:material:1.7.0")

    api(platform("androidx.compose:compose-bom:2023.10.00"))
    api("androidx.compose.material:material")
    api("androidx.compose.ui:ui-tooling-preview")
    debugApi("androidx.compose.ui:ui-tooling")
}
