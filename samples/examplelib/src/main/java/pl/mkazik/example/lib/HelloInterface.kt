package pl.mkazik.example.lib

import pl.mkazik.example.kotlinlib.Car

interface HelloInterface {
    fun say(): String
    fun car(): Car
}
