package pl.mkazik.example.lib

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview

@Composable
fun TestGui(onClick: () -> Unit) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = "Hello world", style = MaterialTheme.typography.h3)
        Button(onClick = onClick) {
            Text(text = "Click me")
        }
    }
}

@Preview(showSystemUi = true, device = Devices.PIXEL)
@Composable
private fun PreviewGui() {
    TestGui {}
}
