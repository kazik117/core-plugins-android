package pl.mkazik.example.lib

import pl.mkazik.example.kotlinlib.Car
import pl.mkazik.example.kotlinlib.Engine

internal class HelloWorld : HelloInterface {
    private val models = listOf("mustang", "panamera", "fiesta", "corolla")

    override fun say(): String = "World!"

    override fun car(): Car = Car(models.random(), Engine.entries.random())
}
