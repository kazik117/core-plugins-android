package pl.mkazik.example.app

import androidx.activity.ComponentActivity
import android.os.Bundle
import android.widget.Toast
import pl.mkazik.example.app.databinding.ActivityMainBinding
import pl.mkazik.example.lib.HelloFactory
import pl.mkazik.example.lib.TestGui

class MainActivity : ComponentActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val helloObj = HelloFactory.newInstance()

        binding.sampleTextView.text = getString(
            R.string.hello_world,
            helloObj.say()
        )

        val car = helloObj.car()
        binding.car.text = getString(R.string.random_car, car.model, car.engine.toString())

        binding.composeView.setContent {
            TestGui {
                Toast.makeText(this, "Hello from compose", Toast.LENGTH_LONG).show()
            }
        }
    }
}
