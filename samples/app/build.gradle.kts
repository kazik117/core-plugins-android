plugins {
    id("pl.mkazik.core.android-app")
    id("pl.mkazik.core.compose")
}

android {
    namespace = "pl.mkazik.example.app"

    buildFeatures {
        viewBinding = true
    }

    buildTypes {
        getByName("release") {
            signingConfig = signingConfigs.getByName("debug")
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
}

dependencies {
    implementation("androidx.activity:activity-ktx:1.7.2")
    implementation(project(":samples:examplelib"))
}
